service_account = "./credentials/observability_306006_93d312b644c9"
#meshantra_service_account = "meshantra_user"

################ project #########################
project_name        = "Meshantra-dev2"
project_id          = "steerwise-meshantra-dev2"
org_id              = 43131877082
billing_account     = "014190-20765A-B67178"
auto_create_network = false
################################################



########### main service account ###################
service_accounts = [{
  service_account_id   = "meshantra-sa"
  service_account_name = "meshantra_Service_Account"
},
{
  service_account_id   = "function-dev"
  service_account_name = "function_Dev_Account"
}]
service_account_id   = "meshantra-sa"
service_account_name = "meshantra_Service_Account"
key_filename         = "meshantra_user.json"
role_sa              = ["roles/dns.admin", "roles/storage.admin", "roles/compute.loadBalancerAdmin","roles/compute.networkAdmin","roles/compute.securityAdmin","roles/cloudfunctions.admin","roles/iam.serviceAccountUser","roles/vpcaccess.admin","roles/compute.publicIpAdmin","roles/cloudsql.admin","roles/endpoints.portalAdmin","roles/servicemanagement.admin"]
######################################################


########## API list for this project ##########,""
project_api = [
  "compute.googleapis.com",
  "iam.googleapis.com",
  "apigateway.googleapis.com",
  "dns.googleapis.com",
  "vpcaccess.googleapis.com",
  "sqladmin.googleapis.com",
  "servicenetworking.googleapis.com",
  "cloudresourcemanager.googleapis.com"
  
]

//////////////////// private dns zone ///////////////

dns_zone_details = [{
  dns_zone_name          = "meshantra-zone5"
  dns_name               = "meshantra.com."
  dns_visibility_network = ""
  }, {
  dns_zone_name          = "meshantra-zone6"
  dns_name               = "meshantra.net."
  dns_visibility_network = ""
}]

////////////////////////////////////////////////////////

//////////////////////// public dns zone ///////////////

public_dns_zone_details = [{
  dns_zone_name = "meshantra-zone1"
  dns_name      = "meshantra.com."
  }, {
  dns_zone_name = "meshantra-zone2"
  dns_name      = "meshantra.net."
}]

/////////////////////////////////////////////////////////



