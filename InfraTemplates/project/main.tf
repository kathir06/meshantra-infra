# resource "google_project" "gcp_project" {
#   name            = var.project_name
#   project_id      = var.project_id
#   org_id          = var.org_id
#   billing_account = var.billing_account
#   auto_create_network = var.auto_create_network
# }

# resource "google_service_account_iam_member" "service_account_observability" {
#    service_account_id = "projects/-/serviceAccounts/terraform@observability-306006.iam.gserviceaccount.com"
#    role               = "roles/serviceAccounts.createServiceAccounts"
#    member             = "serviceAccount:terraform@observability-306006.iam.gserviceaccount.com"

#  }

module "project-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "10.1.1"

  project_id = var.project_id

  activate_apis = var.project_api

}

resource "google_service_account" "gcp_service_account" {
  count = length(var.service_accounts)
  account_id   = var.service_accounts[count.index].service_account_id
  display_name = var.service_accounts[count.index].service_account_name
  project      = var.project_id
}

resource "google_project_iam_member" "service_account_IAM" {
  count   = length(var.role_sa)
  role    = var.role_sa[count.index]
  member  = "serviceAccount:${google_service_account.gcp_service_account[0].email}"
  project = var.project_id
}

resource "google_service_account_key" "generate_service_key" {
  service_account_id = google_service_account.gcp_service_account[0].name
}

resource "local_file" "download_key" {
  content  = base64decode(google_service_account_key.generate_service_key.private_key)
  filename = "./credentials/${var.key_filename}"
}

################## DNS Zone ##########################
module "private_dns_zone" {
  source = "./dns_zone/private"
  dns_zone_details = var.dns_zone_details
  project_id = var.project_id
}


module "public_dns_zone" {
  source = "./dns_zone/public"
  dns_zone_details = var.public_dns_zone_details
  project_id = var.project_id
}







