terraform {
  backend "gcs" {
    bucket = "backend_meshantra_statefile"
    prefix = "dev/resources"
    credentials = "../project/credentials/meshantra_user.json"
  }
}