resource "google_vpc_access_connector" "connector" {
  name          = var.connector_name
  ip_cidr_range = var.connector_ip_range
  network       = var.network_name
  project = var.project_id
  region = var.region_name

}

resource "google_storage_bucket" "bucket" {
  name = var.bucket_name_function
  project = var.project_id
  location = var.bucket_location
}

resource "google_storage_default_object_access_control" "bucket_website_access" {
  bucket = google_storage_bucket.bucket.name
  role   = var.bucket_role
  entity = var.bucket_access
}

resource "google_storage_bucket_object" "archive" {
  name   = var.object_name
  bucket = google_storage_bucket.bucket.name
  source = var.object_source_path
}

resource "google_cloudfunctions_function" "function" {
  name        = var.function_name
  description = var.function_description
  runtime     = var.function_runtime
  project = var.project_id

  available_memory_mb   = var.available_memory_mb
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = var.trigger_http
  entry_point           = var.entry_point
  ingress_settings = "ALLOW_INTERNAL_ONLY"
  region = var.region_name
  vpc_connector = "projects/${var.project_name}/locations/${var.region_name}/connectors/vpc-dev"
  vpc_connector_egress_settings = "PRIVATE_RANGES_ONLY"
}

resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = var.project_id
  region         = var.region_name
  cloud_function = google_cloudfunctions_function.function.name

  role   = var.function_role
  member = var.function_member
}

