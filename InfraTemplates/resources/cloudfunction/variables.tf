variable "project_name" {
    type = string
    description = "project name for function"
}

variable "bucket_name_function" {
    type = string
    description = "bucket name for function"
}

variable "bucket_location" {
    type = string
    description = "bucket location"
}

variable "bucket_role" {
    type = string
    description = "bucket role"
}

variable "bucket_access" {
    type = string
    description = "bucket access"
}

variable "object_name" {
    type = string
    description = "object_name"
}

variable "object_source_path" {
    type = string
    description = "object source path"
}

variable "network_name" {
    type = string
    description = "this is network name for function"
}

variable "function_name" {
    type = string
    description = "function name"
}

variable "function_description" {
    type = string
    description = "function description"
}

variable "function_runtime" {
    type = string
    description = "function runtime"
}

variable "available_memory_mb" {
    type = string
    description = "available memory mb"
}

variable "trigger_http" {
    type = string
    description = "trigger http"
}

variable "entry_point" {
    type = string
    description = "entry point"
}

variable "function_role" {
    type = string
    description = "function role"
}

variable "function_member" {
    type = string
    description = "function member"
}

variable "project_id" {
    type = string
    description = "project id"
}

variable "region_name" {
    type = string
    description = "region name"
}


variable "connector_name" {
    type = string
    description = "This is connector name"
}

variable "connector_ip_range" {
    type = string
    description = "This is connector name"
}

