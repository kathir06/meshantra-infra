variable "module_source" {

  description = "This variable is to map the respective module's source"

  type        = string

}

variable "k8s_project_id" {

  description = "The project ID to host the cluster in (required)"

  type        = string

}

variable "name" {

  description = "The name of the cluster (required)"

  type        = string

}

variable "kubernetes_version" {

  description = "The Kubernetes version of the masters. If set to 'latest' it will pull latest available version in the selected region."

  type        = string

}

variable "region" {

  description = "The region to host the cluster in (optional if zonal cluster / required if regional)	"

  type        = string

}

variable "zones" {

  description = "The zones to host the cluster in (optional if regional cluster / required if zonal)"

  type        = list(string)

}

variable "network" {

  description = "The VPC network to host the cluster in (required)"

  type        = string

}

variable "subnetwork" {

  description = "The subnetwork to host the cluster in (required)"

  type        = string

}

variable "ip_range_pods" {

  description = "The name of the secondary subnet ip range to use for pods"

  type        = string

}

variable "ip_range_services" {

  description = "The name of the secondary subnet range to use for services"

  type        = string

}

variable "http_load_balancing" {

  description = "Enable httpload balancer addon"

  type        = bool

}

variable "horizontal_pod_autoscaling" {

  description = "Enable horizontal pod autoscaling addon"

  type        = bool

}

variable "network_policy" {

  description = "Enable network policy addon"

  type        = bool

}



variable "node_pools" {

  description = "List of maps containing node pools"

  type        = list(map(string))

}



variable "node_pools_labels" {

  description = "Map of maps containing node labels by node-pool name"

  type        = map(map(string))



}

variable "horizontal_pod_autoscaling" {

  description = "Enable horizontal pod autoscaling addon"

  type        = bool

}

variable "network_policy" {

  description = "Enable network policy addon"

  type        = bool

}



variable "node_pools" {

  description = "List of maps containing node pools"

  type        = list(map(string))

}



variable "node_pools_labels" {

  description = "Map of maps containing node labels by node-pool name"

  type        = map(map(string))



}

