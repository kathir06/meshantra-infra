variable "project_id" {
  description = "This is the id of the project created in GCP"
  type        = string

}


variable "security_policy_name" {
  description = "THis is the name of the security policy created"
  type        = string
}



variable "security_policy_details" {
  description = "This contains the condition to check and action to be done (either allow or deny requests)"
  type = list(object({
    security_action    = string
    security_priority  = string
    security_source_ip = string
  }))
}

