
resource "google_dns_record_set" "dns_record_set" {
  count        = length(var.dns_record_set_details)
  managed_zone = var.dns_record_set_details[count.index].dns_zone_name
  name         = var.dns_record_set_details[count.index].dns_name
  type         = var.dns_record_set_details[count.index].resource_record_type
  rrdatas      = [var.dns_record_set_details[count.index].resource_record_data]
  ttl          = var.dns_record_set_details[count.index].resource_record_ttl
  project      = var.project_id
}

resource "google_storage_bucket" "bucket_website" {
  name     = var.storage_bucket_name
  location = var.storage_bucket_location
  project  = var.project_id

  website {
    main_page_suffix = var.storage_main_page_suffix
    not_found_page   = var.storage_not_found_page
  }

}

resource "google_storage_default_object_access_control" "bucket_website_access" {
  count  = length(var.storage_bucket_access)
  bucket = var.storage_bucket_access[count.index].bucket_name
  role   = var.storage_bucket_access[count.index].access_level
  entity = var.storage_bucket_access[count.index].access_member
}

resource "google_compute_global_address" "external_ip" {
  project = var.project_id
  name    = var.external_lb
}

resource "google_compute_backend_bucket" "bucket_backend" {
  project     = var.project_id
  name        = var.backend_bucket
  bucket_name = google_storage_bucket.bucket_website.name
  enable_cdn  = true
}



resource "google_compute_url_map" "website_url_map" {
  project         = var.project_id
  name            = var.url_map_name
  default_service = google_compute_backend_bucket.bucket_backend.self_link
}


resource "google_compute_target_http_proxy" "website_proxy" {
  project = var.project_id
  name    = var.target_proxy_name
  url_map = google_compute_url_map.website_url_map.self_link
  
}

resource "google_compute_global_forwarding_rule" "default_forwarding_rule" {
  project               = var.project_id
  name                  = var.forwarding_rule_name
  load_balancing_scheme = "EXTERNAL"
  ip_address            = google_compute_global_address.external_ip.address
  ip_protocol           = "TCP"
  port_range            = "80"
  target                = google_compute_target_http_proxy.website_proxy.self_link
}


