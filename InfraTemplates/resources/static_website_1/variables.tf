variable "project_id" {
  description = "This is the id of the project created in GCP"
  type        = string

}

variable "meshantra_service_account" {
  description = "newly created  service account for this project"
  type        = string
}



variable "dns_record_set_details" {
  description = "This is the DNS record set that is created"
  type = list(object({ dns_zone_name = string
    dns_name             = string
    resource_record_type = string
    resource_record_data = string
    resource_record_ttl  = string
  }))
}

variable "storage_bucket_name" {
  description = "This is name of the storage bucket created"
  type        = string

}


variable "storage_bucket_location" {
  description = "This is the location on which the storage bucket is created"
  type        = string

}


variable "storage_main_page_suffix" {
  description = "This is the main page suffix (index.html) of the website that is created"
  type        = string

}


variable "storage_not_found_page" {
  description = "This is the not found page of the website that is created"
  type        = string

}

variable "storage_bucket_access" {
  type = list(object({ bucket_name = string
    access_level  = string
    access_member = string
  }))
  description = "The level of access and for whom it should be given to"
}

variable "external_lb" {
  description = "This the the external ip address of the load balancer"
  type        = string
}

variable "backend_bucket" {
  description = "This the the external ip address of the load balancer"
  type        = string
}


variable "url_map_name" {
  description = "This the the name of the url map that is created "
  type        = string
}

variable "target_proxy_name" {
  description = " This the the name of https target proxy that is created "
  type        = string
}

variable "forwarding_rule_name" {
  description = " This the the name of http forwarding rule that is created "
  type        = string
}

