
project_id                = "steerwise-meshantra-dev2"
meshantra_service_account = "meshantra_user"

dns_record_set_details = [{
  dns_zone_name        = "meshantra-zone1"
  dns_name             = "meshantra.com."
  resource_record_type = "A"
  resource_record_data = "34.117.104.22"
  resource_record_ttl  = "360"
  }
  , {
    dns_zone_name        = "meshantra-zone2"
    dns_name             = "meshantra.net."
    resource_record_type = "A"
    resource_record_data = "192.0.2.21"
    resource_record_ttl  = "360"
  }
]

############### storage #######################


storage_bucket_name      = "meshantra-bucket2"
storage_bucket_location  = "US"
storage_main_page_suffix = "index.html"
storage_not_found_page   = ""


storage_bucket_access = [{
  bucket_name   = "meshantra-bucket2"
  access_level  = "READER"
  access_member = "allUsers"
}]

external_lb    = "load-balancer-ip2"
backend_bucket = "website-backend2"




url_map_name         = "website-url-map"
target_proxy_name    = "website-target-proxy"
forwarding_rule_name = "website-forwarding-rule"
