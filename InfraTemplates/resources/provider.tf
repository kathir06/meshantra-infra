terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
    google-beta = {
      source = "hashicorp/google-beta"
    }
  }
}

provider "google" {
  credentials = file("${var.service_account}")
}

provider "google-beta" {

  credentials = file("${var.service_account}")

}
