

locals {
    network_tag = "projects/${var.project_id}/global/networks/${var.network_name}"
}

resource "google_compute_global_address" "private_ip_block" {
  project      = var.project_id
  name         = var.private_name
  purpose      = var.private_purpose
  address_type = var.private_address_type
  prefix_length = var.private_prefix_length
  network       = local.network_tag
}

resource "google_service_networking_connection" "private_vpc_connection" {
  
  network                 = local.network_tag
  service                 = var.peer_connection_service
  reserved_peering_ranges = [google_compute_global_address.private_ip_block.name]
}




resource "google_sql_database_instance" "instance" {
  
  project             = var.project_id
  name                = var.sql_name
  database_version    = var.database_version
  region              = var.region_name
  #encryption_key_name = var.encryption_key_name
  deletion_protection = var.deletion_protection


  settings {
    tier                        = var.db_tier
    activation_policy           = var.activation_policy
    availability_type           = var.availability_type
    dynamic "backup_configuration" {
      for_each = var.backup_configuration.enabled ? [var.backup_configuration] : []
      content {
        enabled                        = lookup(backup_configuration.value, "enabled", null)
        start_time                     = lookup(backup_configuration.value, "start_time", null)
        point_in_time_recovery_enabled = lookup(backup_configuration.value, "point_in_time_recovery_enabled", null)
      }
      
    }

    ip_configuration {
      ipv4_enabled    = false
      private_network = local.network_tag
    }

    
    disk_autoresize = var.disk_autoresize
    disk_size       = var.disk_size
    disk_type       = var.disk_type
    pricing_plan    = var.pricing_plan
    dynamic "database_flags" {
        for_each = var.database_flags
      content {
        name  = lookup(database_flags.value, "name", null)
        value = lookup(database_flags.value, "value", null)
      }
    }

   # user_labels = var.user_labels

    location_preference {
      zone = var.zone
    }

    maintenance_window {
      day          = var.maintenance_window_day
      hour         = var.maintenance_window_hour
      update_track = var.maintenance_window_update_track
    }
  }

  lifecycle {
    ignore_changes = [
      settings[0].disk_size
    ]
  }

}


resource "google_sql_user" "users" {
  project = var.project_id
  name     = var.db_username
  instance = google_sql_database_instance.instance.name
  password = var.db_password
}

