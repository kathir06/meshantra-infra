variable "network_name" {
    type = string
    description = "this is vpc network name"
}

variable "project_id" {
    type = string
    description = "this is project id"
}

variable "private_name" {
    type = string
    description = "this is private name"
}

variable "private_purpose" {
    type = string
    description = "this is private name"
}

variable "private_address_type" {
    type = string
    description = "this is private address type"
}

variable "private_prefix_length" {
    type = string
    description = "this is private prefix length"
}

///////////// peering connection /////////////

variable "peer_connection_service" {
    type = string
    description = "this is private address type"
}


//////////// cloud sql //////////////////////

variable "sql_name" {
    type = string
    description = "this is cloud sql instance name"
}

variable "database_version" {
    type = string
    description = "this is cloud sql database version"
}

variable "region_name" {
    type = string
    description = "this is cloud sql region name"
}

variable "deletion_protection" {
    type = string
    description = "this is cloud sql region name"
}

variable "db_tier" {
    type = string
    description = "this is db tier type"
}

variable "activation_policy" {
    type = string
    description = "this is activation policyname"
}

variable "availability_type" {
    type = string
    description = "this is availability type name"
}

variable "disk_autoresize" {
    type = string
    description = "this is disk autoresize status value"
}

variable "disk_size" {
    type = string
    description = "this is disk size of instance"
}

variable "disk_type" {
    type = string
    description = "this is disk type of instance"
}

variable "pricing_plan" {
    type = string
    description = "this is pricing plan of database"
} 

variable "user_labels" {
    type = string
    description = "this is custom user label"
} 

variable "zone" {
    type = string
    description = "this is name of zone"
}

variable "backup_configuration" {
    type = map
    description = "this is name of zone"
} 


variable "maintenance_window_day" {
    type = number
    description = "this is windows day for maintenance"
} 

variable "maintenance_window_hour" {
    type = number
    description = "this is windows hour for maintenance"
} 

variable "maintenance_window_update_track" {
    type = string
    description = "this is windows update track for maintenance"
}


variable "db_username" {
    type = string
    sensitive = true
    description = "this is username of DB"
}

variable "db_password" {
    type = string
    sensitive = true
    description = "this is password of DB"
}

variable "database_flags" {
    type = list
    description = "this is database flag"
}









/////////////////////////////////////////////


