# ###################### Armor ###########################

# module "create_armor" {
#   source = "./armor"
#   project_id = var.project_id
#   security_policy_details = var.security_policy_details
#   security_policy_name = var.security_policy_name
# }


# ###################### static website ####################


# module "static_website" {
#   source = "./static_website_1"
#   project_id = var.project_id
#   dns_record_set_details = var.dns_record_set_details
#   storage_bucket_name = var.storage_bucket_name
#   storage_bucket_location = var.storage_bucket_location
#   storage_bucket_access = var.storage_bucket_access
#   external_lb = var.external_lb
#   backend_bucket = var.backend_bucket
# }




################## vpc - subnet - routes - firewall ######################
module "create_vpc" {
  source = "./vpc"
  project_id = var.project_id
  auto_create_subnetworks = var.auto_create_subnetworks
  network_name = var.network_name
  routing_mode = var.routing_mode
  firewall_rules = var.firewall_rules
  delete_default_internet_gateway_routes = var.delete_default_internet_gateway_routes
  mtu = var.mtu
  shared_vpc_host = var.shared_vpc_host
  subnets = var.subnets
  secondary_ranges = var.secondary_ranges
  routes = var.routes
}


# ################### router and natgateway ######################
# module "create_router_and_nat" {
#     #-------------------Cloud Router-----------------------#
#   source         = "./cloudRouter"
#   project_id     = var.project_id
#   router_name    = var.router_name
#   asn_value      = var.asn_value
#   advertise_mode = var.advertise_mode
#   region_name    = var.region_name
#   network_name   = var.network_name

#   #--------------------Nat Gateway------------------------#
#   nat_name                    = var.nat_name
#   nat_ip_allocate_option             = var.nat_ip_allocate_option
#   source_subnetwork_ip_ranges_to_nat = var.source_subnetwork_ip_ranges_to_nat
#   subnetworks         = var.subnetworks
#   min_ports_per_vm                = var.min_ports_per_vm

#   depends_on = [
#     module.create_vpc.network_name
#   ]

# }

################# cloud function ######################
# module "cloud_function" {

#   source = "./cloudfunction"
#   //cloud vpc serverless connector
#   connector_name = var.connector_name
#   connector_ip_range = var.connector_ip_range
#   network_name = var.network_name

#   //bucket detail
#   bucket_name_function = var.bucket_name_function
#   project_id = var.project_id
#   project_name = var.project_name
#   bucket_location = var.bucket_location
#   bucket_role = var.bucket_role
#   bucket_access = var.bucket_access
#   //achrive object from zip file
#   object_name = var.object_name
#   object_source_path = var.object_source_path
#   //function parameters
#   function_name = var.function_name
#   function_description = var.function_description
#   function_runtime = var.function_runtime
#   available_memory_mb = var.available_memory_mb
#   trigger_http = var.trigger_http
#   entry_point = var.entry_point
#   function_role = var.function_role
#   function_member = var.function_member
#   region_name = var.region_name
# }

module "endpoint" {
  source = "./endpoints"
  service_run_name = var.service_run_name
  service_account_name = var.service_account_name
  service_image = var.service_image
  service_env_name = var.service_env_name
  service_env_value = var.service_env_value
  project_id = var.project_id
  region_name = var.region_name
}

# module "create_sql" {
#   source = "./cloud_sql"
#   network_name = var.network_name
#   project_id = var.project_id
#   /// private ip 
#   private_name = var.private_name
#   private_purpose = var.private_purpose
#   private_address_type = var.private_address_type
#   private_prefix_length = var.private_prefix_length

#   /////////////// vpc peer ///////////////
#   peer_connection_service = var.peer_connection_service

#   //////////// cloud sql /////////////
#   sql_name = var.sql_name
#   database_version = var.database_version
#   region_name = var.region_name
#   deletion_protection = var.deletion_protection
#   db_tier = var.db_tier
#   activation_policy = var.activation_policy
#   availability_type = var.availability_type
#   disk_autoresize = var.disk_autoresize
#   disk_size = var.disk_size
#   disk_type = var.disk_type
#   pricing_plan = var.pricing_plan
#   user_labels = var.user_labels
#   zone = var.zone
#   backup_configuration = var.backup_configuration
#   maintenance_window_day = var.maintenance_window_day
#   maintenance_window_hour = var.maintenance_window_hour
#   maintenance_window_update_track = var.maintenance_window_update_track
#   db_username = var.db_username
#   db_password = var.db_password
#   database_flags = var.database_flags

  
# }

# module "create_run" {
#   source = "./cloud_run"
# }


###################### Kubernetes #############################




# google_client_config and kubernetes provider must be explicitly specified like the following.

data "google_client_config" "default" {}


provider "kubernetes" {

  #load_config_file       = false

  host                   = "https://${module.gke.endpoint}"

  token                  = data.google_client_config.default.access_token

  cluster_ca_certificate = base64decode(module.gke.ca_certificate)

}


module "gke" {

  source                     = "terraform-google-modules/kubernetes-engine/google"

  project_id                 = var.k8s_project_id

  name                       = var.name

  region                     = var.region

  zones                      = var.zones

  network                    = var.network

  subnetwork                 = var.subnetwork

  ip_range_pods              = var.ip_range_pods

  ip_range_services          = var.ip_range_services

  http_load_balancing        = var.http_load_balancing

  horizontal_pod_autoscaling = var.horizontal_pod_autoscaling

    network_policy             = var.network_policy

  kubernetes_version         = var.kubernetes_version


  node_pools = var.node_pools


  node_pools_oauth_scopes = var.node_pools_oauth_scopes


  node_pools_labels = var.node_pools_labels


  node_pools_metadata = var.node_pools_metadata


#   node_pools_taints = var.node_pools_taints


  node_pools_tags = var.node_pools_tags

}






