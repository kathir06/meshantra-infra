
resource "google_cloud_run_service" "api_function_proxy" {
  name     = var.service_run_name
  location = var.region_name
  project = var.project_id
  

  template {
    spec {
      service_account_name = var.service_account_name
      containers {
        # Default to the unmodified image on first deploy
        image = var.service_image

        env {
          name  = var.service_env_name
          value = var.service_env_value
        }
      }
    }
  }
}

output "test" {
  value = google_cloud_run_service.api_function_proxy.status[0].url
}

  data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location    = google_cloud_run_service.api_function_proxy.location
  project     = google_cloud_run_service.api_function_proxy.project
  service     = google_cloud_run_service.api_function_proxy.name

  policy_data = data.google_iam_policy.noauth.policy_data
}

data "template_file" "open_api" {
   template = file("${path.module}/open-api.yaml")
   vars = {
     hostname =  replace(google_cloud_run_service.api_function_proxy.status[0].url,"https://","")
   }
}

resource "google_endpoints_service" "openapi_service" {
  service_name   = replace(google_cloud_run_service.api_function_proxy.status[0].url,"https://","")
  project        = "steerwise-meshantra-dev2"
  openapi_config = data.template_file.open_api.rendered

  depends_on = [
    google_cloud_run_service.api_function_proxy
  ]
}

# resource "google_cloudbuild_trigger" "filename-trigger" {

#   filename = "cloudbuild.yaml"
# }



 