//////////////////////////// project ///////////////////
service_account = "../project/credentials/meshantra_user.json"
project_id = "steerwise-meshantra-dev2"
project_name = "Meshantra-dev2"
region_name = "us-central1"


//////////////////// private dns zone ///////////////

dns_zone_details = [{
  dns_zone_name          = "meshantra-zone5"
  dns_name               = "meshantra.com."
  dns_visibility_network = ""
  }, {
  dns_zone_name          = "meshantra-zone6"
  dns_name               = "meshantra.net."
  dns_visibility_network = ""
}]

////////////////////////////////////////////////////////

//////////////////////// public dns zone ///////////////

public_dns_zone_details = [{
  dns_zone_name = "meshantra-zone1"
  dns_name      = "meshantra.com."
  }, {
  dns_zone_name = "meshantra-zone2"
  dns_name      = "meshantra.net."
}]

/////////////////////////////////////////////////////////

//////////////////// Armor ////////////////////////////

security_policy_name = "testpolicy5"
security_policy_details = [{

  security_action    = "deny(404)"
  security_priority  = 1000
  security_source_ip = "1.1.3.0/24"
  }]

//////////////////////////////////////////////////////


////////////////////// static website/////////////////

dns_record_set_details = [{
  dns_zone_name        = "meshantra-testzone2"
  dns_name             = "meshantra.com."
  resource_record_type = "A"
  resource_record_data = "34.102.245.97"
  resource_record_ttl  = "360"
  }
  , {
    dns_zone_name        = "meshantra-testzone3"
    dns_name             = "meshantra.net."
    resource_record_type = "A"
    resource_record_data = "192.0.2.21"
    resource_record_ttl  = "360"
  }
]

############### storage #######################


storage_bucket_name     = "meshantra-bucket1"
storage_bucket_location = "US"


storage_bucket_access = [{
  bucket_name   = "meshantra-bucket1"
  access_level  = "OWNER"
  access_member = "allUsers"
}]

external_lb    = "load-balancer-ip"
backend_bucket = "website-backend"


////////////////////////////////////////////////////////


////////////////////// vpc //////////////////////////

network_name            = "vpc-dev"
routing_mode            = "GLOBAL"
auto_create_subnetworks = "false"
mtu = 1500
delete_default_internet_gateway_routes = false
shared_vpc_host = false
subnets = [{
    subnet_name = "subnet-01-private"
    subnet_ip  = "10.10.0.0/16"
    subnet_region = "us-central1"
    subnet_private_access = "true"
    description = "private subnet 1"
},
{
    subnet_name = "subnet-02-private"
    subnet_ip  = "10.11.0.0/16"
    subnet_region = "us-central1"
    subnet_private_access = "true"
    description = "private subnet 2"
},
{
    subnet_name = "subnet-03-public"
    subnet_ip  = "10.12.1.0/24"
    subnet_region = "us-central1"
    description = "public subnet 1"
}]
secondary_ranges = { 
    subnet-01-private = [
            {
                range_name    = "subnet-01-secondary-01"
                ip_cidr_range = "192.168.64.0/24"
            },
        ]
        subnet-02-private = []
}

routes = []

firewall_rules = [{
    name                    = "allow-ssh-ingress"
    description             = null
    direction               = "INGRESS"
    priority                = null
    ranges                  = ["0.0.0.0/0"]
    source_tags             = null
    source_service_accounts = null
    target_tags             = null
    target_service_accounts = null
    allow = [{
      protocol = "tcp"
      ports    = ["22"]
    }]
    deny = []
    log_config = {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }]



////////////////////////////////////////////////////////


//////////////////////////cloud nat and routers//////////

router_name    = "router-dev"
asn_value      = "64514"
advertise_mode = "CUSTOM"


nat_name                    = "router-nat"
min_ports_per_vm = "64"
nat_ip_allocate_option             = "AUTO_ONLY"
source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
subnetworks         = [{
  name = "subnet-01-private"
  source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  secondary_ip_range_names = []
},{
  name = "subnet-02-private"
  source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  secondary_ip_range_names = []
}]


/////////////////////////////////////////////////////////////////////////

/////////////////////////////// cloud function /////////////////////////

bucket_name_function = "test-bucket-function-dev"
bucket_location = "US"
bucket_role = "OWNER"
bucket_access = "allUsers"
object_name = "index"
object_source_path = "./code/index.zip"

function_name= "function-dev"
function_description = "My function"
function_runtime = "nodejs14"
available_memory_mb = "128"
trigger_http = "true"
entry_point = "hello"
function_role = "roles/cloudfunctions.invoker"
function_member = "serviceAccount:function-dev@steerwise-meshantra-dev2.iam.gserviceaccount.com"
connector_name = "vpc-con"
connector_ip_range = "10.8.0.0/28"

/////////////////////////////////////////////////////////////////////////


///////////// cloud run //////////////////////////////////
service_run_name = "my-api-function-proxy5"
service_account_name = "function-dev@steerwise-meshantra-dev2.iam.gserviceaccount.com"
service_image = "gcr.io/endpoints-release/endpoints-runtime-serverless:2"
service_env_name = "ESPv2_ARGS"
service_env_value = "^++^--cors_preset=basic++--backend_dns_lookup_family=v4only"

///////////////////////////////////////////////////////

/////////////////// private ip and cloud sql //////////////////////////////

private_name = "private-ip-block"
private_purpose = "VPC_PEERING"
private_address_type = "INTERNAL"
private_prefix_length = "20"


/////////////////////   peer connection //////////////////////////////

peer_connection_service = "servicenetworking.googleapis.com"

///////////////////// cloud sql ////////////////////////////////////


sql_name = "instance-2"
database_version = "POSTGRES_13"
deletion_protection = "false"
db_tier = "db-f1-micro"
activation_policy = "ON_DEMAND"
availability_type = "REGIONAL"
disk_autoresize = "true"
disk_size = "10"
disk_type = "PD_SSD"
pricing_plan = "PER_USE"
user_labels = "" 
zone = "us-central1-a"
backup_configuration = {
  enabled = "true"
  start_time = "01:00"
  point_in_time_recovery_enabled = "true"
}
maintenance_window_day = 1
maintenance_window_hour = 1
maintenance_window_update_track = "stable"
database_flags = []

// postgress user
db_username = "admin"
db_password = "steerwise123!@#"




/////////////////////////////////////////////////////////////////////////////


############### kubernetes #######################



module_source              = "terraform-google-modules/kubernetes-engine/google"

k8s_project_id             = "steerwise-meshantra-dev2"

name                       = "meshantra-gke-test-cluster"

kubernetes_version         = "latest"

region                     = "us-central1"

zones                      = ["us-central1-a", "us-central1-b", "us-central1-f"]

network                    = "test-vpc1"

subnetwork                 = "test-private"

ip_range_pods              = "test-private-gke-01-pods"

ip_range_services          = "test-private-gke-01-services"

http_load_balancing        = false

horizontal_pod_autoscaling = false

network_policy             = false

node_pools = [

    {

      name               = "meshantra-small-node-pool"

      machine_type       = "e2-micro"

      node_locations     = "us-central1-b,us-central1-c"

      min_count          = 1

      max_count          = 2

      local_ssd_count    = 0

      disk_size_gb       = 10

      disk_type          = "pd-standard"

      image_type         = "COS"

      auto_repair        = true

      auto_upgrade       = false

      service_account    = "meshantrasa@steerwise-meshantra-dev2.iam.gserviceaccount.com"

      preemptible        = true

      initial_node_count = 1

    },

  ]

node_pools_oauth_scopes = {

    all = []


    meshantra-small-node-pool = [

      "https://www.googleapis.com/auth/cloud-platform",

    ]

  }


  node_pools_labels = {

    all = {}


    meshantra-small-node-pool = {

      meshantra-small-node-pool = true

      node-pool-env = "dev"


    }

  }


  node_pools_metadata = {

    all = {}


    meshantra-small-node-pool = {

      mesh-name = "mesh1"

    }

  }

  # node_pools_taints = {

  #   all = []


  #   meshantra-small-node-pool = [

  #     {

  #       key    = "meshantra-small-node-pool"

  #       value  = true

  #       effect = "PREFER_NO_SCHEDULE"

  #     },

  #   ]

  # }


  node_pools_tags = {

    all = []


    meshantra-small-node-pool = [

      "dev","practice","gke","rel-1"

    ]

  }