resource "google_vpc_access_connector" "connector" {
  name          = "run-dev"
  ip_cidr_range = "10.8.0.0/28"
  network       = "vpc-dev"
  project = "steerwise-meshantra-dev2"
  region = "us-central1"

}

module cloud_run {
    source = "garbetjie/cloud-run/google"
    name = "my-service"
    image = "gcr.io/cloudrun/hello"
    location = "us-central1"
    allow_public_access = true
    cpus =1
    ingress = "internal"
    max_instances  = 1
    min_instances = 1
    project = "steerwise-meshantra-dev2"
    vpc_access_egress = "private-ranges-only"	
    vpc_connector_name = "projects/steerwise-meshantra-dev2/locations/us-central1/connectors/run-dev"

    depends_on = [google_vpc_access_connector.connector]
}



